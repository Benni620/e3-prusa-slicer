M104 S0 ; turn off temperature
M140 S0; Turn off Bedd
G91; set coordinates to relative
G1 E-4 F900;  Retract Filament to minimize droping filament while still cooling nozzle
G1 F3000 Z10; lift nozzle off the print 10mm
G90; change to absolute
G1 X5.0 F2000
G1 Y2900 F1000;
M84     ; disable motors
M107 ; turn off fan
M300 S100 P500 ; chirp
M77
