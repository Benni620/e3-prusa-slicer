M76
M104 S[first_layer_temperature] ; set extruder temp
M140 S[first_layer_bed_temperature] ; set bed temp
G1 Z20 ; this is a good start heating position
G28; Home XY

@BEDLEVELVISUALIZER
G29; Auto bed level

M190 S[first_layer_bed_temperature] ; wait for bed temp
M109 S[first_layer_temperature] ; wait for extruder temp


G1 X0.0 F7200;
G1 Y0.0 F7200;
G1 Z0.3 F7200;

G1 Y12.0 F3200;

M117 Purge extruder
G92 E0 ; reset extruder

G1 X0.0 Y200.0 Z0.14 F1500.0 E15 ; draw 1st line
G1 X0.4 Y200.0 Z0.14 F5000.0 ; move to side a little
G1 X0.4 Y12.0 Z0.14 F1500.0 E22 ; draw 2nd line
G1 X0.8 Y12.0 Z0.14 F5000.0 ; move to side a little
G92 E0 ; reset extruder
G1 Z3.5 F3000 ; move z up little to prevent scratching of surface
M117 All systems go!
