# Ender 3 Prusa Slicer

Prusa Slicer profile for Ender 3

## Authors and acknowledgment
- Based on this Profile:[ ] [PrusaSlicer Profile for Creality3D Printers](https://www.thingiverse.com/thing:3126776)

## License
[ ] [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

